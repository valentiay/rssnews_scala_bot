package com.gitlab.dyvol777.RSSNews.loaders

import java.nio.charset.{Charset, CodingErrorAction}
import java.text.SimpleDateFormat
import java.util.{Calendar, Date, Locale, TimeZone}

import com.gitlab.dyvol777.RSSNews.Implicits._
import com.gitlab.dyvol777.RSSNews.models.Article
import com.gitlab.dyvol777.RSSNews.utils.DateUtils

import scala.io.{Codec, Source}
import scala.util.{Success, Try}
import scala.xml.XML


object RssLoader {
  //private val rssURI = "https://habr.com/ru/rss/all/all/"
  private val codec = Codec(Charset.forName("UTF-8")).onMalformedInput(CodingErrorAction.IGNORE)

  private def getTextFromUrl(url: String): String = Source.fromURL(url)(codec).use(_.getLines().mkString("\n"))

  /** may block thread or throw exceptions */
  def downloadRSSArticles(url: String): Seq[Article] = parseRss(getTextFromUrl(url))

  def parseRss(text: String): Seq[Article] = {
    val root = XML.loadString(text)
    val items = root \ "channel" \ "item"

    items.toList.map { item =>
      val link = (item \ "guid").text

      Article(
        link = link,
        title = (item \ "title").text,
        description = (item \ "description").text,
        publicationDate = Try(parseDate((item \ "pubDate").text)).getOrElse(Calendar.getInstance().getTime)
      )
    }
  }

  def parseDate(s: String): Date = dateFormat.parse(s)

  private val dateFormat = new SimpleDateFormat("EEE, dd MMM yyy HH:mm:ss 'GMT'", Locale.ENGLISH) {
    setTimeZone(TimeZone.getTimeZone("GMT"))
  }

  private def extractId(link: String): Int =
    link.split("/").filter(_.nonEmpty).last.toInt

  private def extractCompany(url: String): Option[String] = {
    val arr = url.split('/').lift
    arr(4).filter(_ == "company").flatMap(_ => arr(5))
  }
}
