package com.gitlab.dyvol777.RSSNews.actors

import java.util.concurrent.Executors

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import com.bot4s.telegram.methods.{EditMessageText, ParseMode, SendMessage}
import com.bot4s.telegram.models.{ChatId, Message}
import com.gitlab.dyvol777.RSSNews.AppConfig.TgBotActorConfig
import com.gitlab.dyvol777.RSSNews.actors.LibraryActor.{GetNewArticles, GetSettings, Ok, SendArticle, UpdateChat}
import com.gitlab.dyvol777.RSSNews.models.{Article, Chat}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.util.{Failure, Success}


object TgBotActor {
  def props(config: TgBotActorConfig, library: ActorRef) = Props(new TgBotActor(config, library))

  final case class SendMessageToTg(chatId: Long, msg: String)
  final case class ReceiveTgMessage(message: Message)

}

class TgBotActor private(config: TgBotActorConfig, library: ActorRef) extends Actor with ActorLogging {

  import TgBotActor._

  private val threadsCount = 2
  private implicit val executionContext: ExecutionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(threadsCount))

  private val bot = TgBotAdapter(config, self)

  override def preStart(): Unit = {
    context.system.scheduler.schedule(config.chatsUpdateInterval, config.chatsUpdateInterval, library, GetNewArticles())
    bot.run()
    log.info("Bot is running...")
  }

  override def receive: Receive = {

    case SendMessageToTg(chatId, msg) => sendMessageToTg(chatId, msg)
    case SendArticle(chatId, article) => sendMessageToTg(chatId, article.toString)
    case ReceiveTgMessage(msg) => processTgMessage(msg)
    case unknownMessage => log.error(s"TG_Actor unknown message: $unknownMessage")
  }

  private def getFirstToken(text: String): Option[String] =
    text.trim.split(" ").headOption.map(_.toLowerCase.stripPrefix("/"))

  private def processTgMessage(implicit message: Message): Unit = {
    val firstToken = message.text.flatMap(getFirstToken).getOrElse("")
    val chatId = message.chat.id

    firstToken match {
      case "settings" => library ! GetSettings(chatId)
      case "start" | "help" => sendMessageToTg(chatId, helpMsgReplyText)
      case "subscribe" => addSubscription(chatId, message.text.get.split(' ')(1))
      case "unsubscribe" => removeSubscription(chatId, message.text.get.split(' ')(1))
      case _ => log.info(s"unknown text: '${message.text.getOrElse("")}'")
    }
  }

  private def addSubscription(chatId: Long, cmd: String): Unit = {
    implicit val timeout: Timeout = 10.seconds
    if (cmd.nonEmpty) {
      val upd = UpdateChat(chatId, (s: Chat) => Chat(s.id, cmd :: s.subscriptions, s.lastUpdate), needConfirmation = true)
      (library ? upd) map { case Ok => SendMessageToTg(chatId, "ok!") } pipeTo self
    } else {
      self ! SendMessageToTg(chatId, "can't parse commands")
    }
  }

  private def removeSubscription(chatId: Long, cmd: String): Unit = {
    implicit val timeout: Timeout = 10.seconds
    if (cmd.nonEmpty) {
      val upd = UpdateChat(chatId, (s: Chat) => Chat(s.id, s.subscriptions.filter(_!=cmd), s.lastUpdate), needConfirmation = true)
      (library ? upd) map { case Ok => SendMessageToTg(chatId, "ok!") } pipeTo self
    } else {
      self ! SendMessageToTg(chatId, "can't parse commands")
    }
  }


  private def helpMsgReplyText: String =
    s"""Subscription to RSS updates with custom filtering
       |/start | /help - list commands
       |/subscribe - receive new articles from rss
       |/unsubscribe - unsubscribe from receiving articles
       |/settings - print all settings""".stripMargin


  private def sendMessageToTg(chatId: Long, msg: String): Unit =
    bot.request(SendMessage(chatId, msg, parseMode = Some(ParseMode.HTML))).onComplete {
      case Success(_) =>
        log.debug(s"Sended message $msg")
      case Failure(ex) =>
        log.error(s"can't send message: $ex")
    }
}
