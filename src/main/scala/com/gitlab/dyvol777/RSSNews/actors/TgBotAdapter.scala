package com.gitlab.dyvol777.RSSNews.actors

import java.net.{Authenticator, InetSocketAddress, PasswordAuthentication, Proxy}

import akka.actor.ActorRef
import com.bot4s.telegram.api.RequestHandler
import com.bot4s.telegram.api.declarative.Messages
import com.bot4s.telegram.clients.ScalajHttpClient
import com.bot4s.telegram.future.{Polling, TelegramBot}

import com.gitlab.dyvol777.RSSNews.AppConfig
import com.gitlab.dyvol777.RSSNews.AppConfig.TgBotActorConfig
import com.gitlab.dyvol777.RSSNews.actors.TgBotActor.ReceiveTgMessage

import scala.concurrent.{ExecutionContext, Future}

/**
 * resend tg messages to actor
 */

class TgBotAdapter(override val client: RequestHandler[Future], observer: ActorRef) extends TelegramBot with Polling with Messages[Future] {
  onExtMessage { case (message, botUser) =>
    Future {
      observer ! ReceiveTgMessage(message)
    }
  }
}

object TgBotAdapter {
  def apply(config: TgBotActorConfig, observer: ActorRef)(implicit ec: ExecutionContext): TgBotAdapter =
    new TgBotAdapter(new ScalajHttpClient(config.token, makeProxy(config)), observer)

  private def makeProxy(config: TgBotActorConfig): Proxy =
    if (config.proxy.ip.isEmpty)
      Proxy.NO_PROXY
    else {
      Authenticator.setDefault(new Authenticator() {
        override def getPasswordAuthentication: PasswordAuthentication = {
          new PasswordAuthentication(
            AppConfig().tgbot.proxy.user,
            AppConfig().tgbot.proxy.pass.toCharArray)
        }
      })
      new Proxy(
        Proxy.Type.SOCKS,
        InetSocketAddress.createUnresolved(
          config.proxy.ip,
          config.proxy.port))
    }

}
