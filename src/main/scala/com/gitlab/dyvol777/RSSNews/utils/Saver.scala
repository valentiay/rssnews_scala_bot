package com.gitlab.dyvol777.RSSNews.utils

trait Saver[T] {
  def load(): T

  def save(t: T): Unit
}
