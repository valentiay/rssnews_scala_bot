package com.gitlab.dyvol777.RSSNews

import cats.instances.future._
import cats.syntax.functor._
import com.bot4s.telegram.api.RequestHandler
import com.bot4s.telegram.api.declarative.Commands
import com.bot4s.telegram.clients.{FutureSttpClient, ScalajHttpClient}
import com.bot4s.telegram.future.{Polling, TelegramBot}
import slogging.{LogLevel, LoggerConfig, PrintLoggerFactory}

import scala.concurrent.duration.Duration
import scala.util.Try
import scala.concurrent.{Await, Future}
import java.net.{Authenticator, InetSocketAddress, PasswordAuthentication, Proxy}

/** Generates random values.
 */
class RandomBot(val token: String) extends TelegramBot
  with Polling
  with Commands[Future] {

  LoggerConfig.factory = PrintLoggerFactory()
  // set log level, e.g. to TRACE
  LoggerConfig.level = LogLevel.DEBUG

  // Use sttp-based backend
  //implicit val backend = SttpBackends.default
  //override val client: RequestHandler[Future] = new FutureSttpClient(token)

  // Or just the scalaj-http backend
  override val client: RequestHandler[Future] =
    new ScalajHttpClient(
      token,
      new Proxy(
        Proxy.Type.SOCKS,
        InetSocketAddress.createUnresolved(
          AppConfig().tgbot.proxy.ip,
          AppConfig().tgbot.proxy.port)))
  Authenticator.setDefault(new Authenticator() {
    override def getPasswordAuthentication(): PasswordAuthentication = {
      new PasswordAuthentication(
        AppConfig().tgbot.proxy.user,
        AppConfig().tgbot.proxy.pass.toCharArray)
    }
  })


  val rng = new scala.util.Random(System.currentTimeMillis())
  onCommand("coin" or "flip") { implicit msg =>
    println("coin")
    reply(if (rng.nextBoolean()) "Head!" else "Tail!").void
  }
  onCommand('real | 'double | 'float) { implicit msg =>
    reply(rng.nextDouble().toString).void
  }
  onCommand("/dice" | "roll") { implicit msg =>
    reply("⚀⚁⚂⚃⚄⚅"(rng.nextInt(6)).toString).void
  }
  onCommand("random" or "rnd") { implicit msg =>
    withArgs {
      case Seq(Int(n)) if n > 0 =>
        reply(rng.nextInt(n).toString).void
      case _ => reply("Invalid argumentヽ(ಠ_ಠ)ノ").void
    }
  }
  onCommand('choose | 'pick | 'select) { implicit msg =>
    withArgs { args =>
      replyMd(if (args.isEmpty) "No arguments provided." else args(rng.nextInt(args.size))).void
    }
  }

  // Int(n) extractor
  object Int {
    def unapply(s: String): Option[Int] = Try(s.toInt).toOption
  }

}