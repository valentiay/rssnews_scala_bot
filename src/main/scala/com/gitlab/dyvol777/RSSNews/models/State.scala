package com.gitlab.dyvol777.RSSNews.models

import java.io.File

import cats.Eq
import com.gitlab.dyvol777.RSSNews.Implicits._
import io.circe.syntax._
import io.circe.{Encoder, Json, _}

import scala.collection.mutable

object State {

  def empty(): State =
    new State(new mutable.HashMap(), new mutable.HashMap())

  def decode(s: String): State =
    io.circe.parser.decode[State](s).right.get

  def encode(c: State): String =
    c.asJson.toString()

  def load(file: File): State =
    decode(file.text)

  def save(c: State, file: File): Unit =
    file.text = encode(c)

  private implicit val chatDataEncoder: Encoder[State] = (chatData: State) =>
    Json.obj(
      "chats" := chatData.chats,
      "articles" := chatData.articles
    )

  private implicit val chatDataDecoder: Decoder[State] = (c: HCursor) =>
    for {
      chats <- c.get[mutable.HashMap[Long, Chat]]("chats")
      articles <- c.get[mutable.HashMap[String, List[Article]]]("chats")
    } yield new State(chats, articles)

  implicit val chatDataEq: Eq[State] = (x: State, y: State) =>
    x.chats == y.chats
}

class State(val chats: mutable.HashMap[Long, Chat], val articles: mutable.HashMap[String, List[Article]]) {

  def getChat(id: Long): Chat = chats.getOrElse(id, Chat.withDefaultSettings(id))

  def updateChat(chatId: Long)(mapFunc: Chat => Chat): Unit = {
    chats(chatId) = mapFunc(chats.getOrElse(chatId, Chat.withDefaultSettings(chatId)))
    assert(chats(chatId).id == chatId)
  }

  override def toString: String =
    State.encode(this)
}

